#!/bin/bash
#
# OpenXT VGlass
# (c) 2015 Assured Information Security, Inc.
#

VG_SHARE="/usr/share/vglass"

# If the user has booted by selecting the "console access"
# configuration entry on their grub configuration...
if grep -q "no-graphics" /proc/cmdline; then
  echo "Booted in console access mode. "

  # ... only start VGlass if this script was invoked
  # with the --force-start option.
  if [[ $* != *"--force-start"* ]]; then
    echo "Not starting VGlass. You can use --force-start to override this check."
    echo
    echo "(Note that OpenXT's troubleshooting options also disable VM autostart. You may want"
    echo " to start any NDVMs and/or UIVM before starting VGlass.)"
    exit 0
  fi
fi

# As a convenience, try to load IVC, if possible.
modprobe ivc

# If the user selected the "clear disman configuration" boot option,
# we'll clear the disman database keys prior to starting.
if grep -q "reset-disman" /proc/cmdline; then
  echo "Boot argument set; resetting the disman configuration.\n"
  /usr/bin/db-rm /disman
fi

# Wait until XenMgr has come up.
xec > /dev/null
while [ $? -ne 0 ]; do
  sleep 1
  xec > /dev/null
done

# Run the script that will bind each of the passthrough graphics
# cards to pciback.
. ${VG_SHARE}/discrete_gfx_modprobe.sh

# Determine the gpus that the UIVM needs to start on
GPUS=$(xec -o /host get-displayhandler-gpu)
if [ ${#GPUS} -gt 0 ]; then
  # IFS=',' read -ra GPU <<< "$GPUS"
  # for i in "${GPU[@]}"; do
  #   DEVICE="${DEVICE:-""} --devices ${i}"
  # done
  DEVICE="--devices ${GPUS}"
fi

#... and start VGlass.
/usr/bin/glass ${DEVICE:-""} $* &
