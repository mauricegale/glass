//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef GLASS_UTILITIES__H
#define GLASS_UTILITIES__H

// Generic Utility
#include <QDebug>
#include <memory>
#include <utility>
#include <algorithm>
#include <json.hpp>
#include <gsl/gsl>
#include <iostream>

// Geometry
#include <QRegion>
#include <glass_rect.h>
#include <QRectF>
#include <QTransform>
#include <QPoint>
#include <QtMath>

// Qt containers
#include <QList>
#include <QHash>
#include <QMap>
#include <QSet>
#include <QStack>

// Qt Other
#include <QMutex>
#include <QMutexLocker>
#include <QTimer>
#include <QUuid>

// STL Containers
#include <list>
#include <unordered_map>

// Typedefs
using region_t = QRegion;
using rect_t = glass_rect_t;
using rectf_t = QRectF;
using transform_t = QTransform;
using margins_t = QMargins;

using point_t = QPoint;
using input_point_t = QPoint;

using pointf_t = QPointF;

using uuid_t = QUuid;
using desktop_uuid_t = QUuid;
using domid_t = uint16_t;
using qmutex_t = QMutex;
using qtimer_t = QTimer;

using json = nlohmann::json;

template<typename T>
using qlist_t = QList<T>;

template<typename T>
using qset_t = QSet<T>;

template<typename T>
using qstack_t = QStack<T>;

template<typename K, typename T>
using qhash_t = QHash<K, T>;

template<typename K, typename T>
using qmap_t = QMap<K, T>;

template<typename T>
using list_t = std::list<T>;

template<typename K, typename T>
using hash_t = std::unordered_map<K, T>;

#ifdef DEBUG
#define vg_debug qDebug
#define DTRACE __PRETTY_FUNCTION__ << ":" << __LINE__
#define TRACE vg_info() << __PRETTY_FUNCTION__ << ":" << __LINE__
#else
#define vg_debug
#endif

#ifdef QUIET
#define vg_info
#define vg_warning
#ifdef VERY_QUIET // Dangerous
#define vg_critical
#define vg_fatal
#else // Safer
#define vg_critical qCritical
#define vg_fatal qFatal
#endif
#else // Noisy
#define vg_info qInfo
#define vg_warning qWarning
#define vg_critical qCritical
#endif

uint qHash(QSize key);

namespace std {
  template <>
    struct hash<uuid_t>
    {
      std::size_t operator()(const uuid_t& k) const
        {
          using std::size_t;

          // Compute individual hash values for first,
          // second and third and combine them using XOR
          // and bit shifting:

          return (qHash(k));
        }
    };

}

#endif //GLASS_TYPES__H
