QT       += core dbus gui

include(../../common_include.prx)

TARGET = $$VG_BASE_DIR/bin/glass
TEMPLATE = app

CONFIG += c++17

SOURCES += glass.cpp
HEADERS += ../../common/include/logging.h

INCLUDEPATH += "$$(STAGING_DIR_TARGET)/usr/include/drm/"

#Implementation specifics
INCLUDEPATH += "../../window_manager/fs_window_manager"
INCLUDEPATH += "../../renderer/dumb_renderer"
INCLUDEPATH += "../../input/input_server"
INCLUDEPATH += "../../toolstack/xenmgr"
INCLUDEPATH += "../.."

LIBS += -L$$VG_BASE_DIR/lib -lvginput -linput -ldumb_renderer -lfs_wm -lxenmgr_toolstack -livc -lxenbackend

target.path = /usr/bin
INSTALLS += target
