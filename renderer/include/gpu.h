//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef GPU__H
#define GPU__H

#include <glass_types.h>

#include <desktop_plane.h>

class gpu_t
{
public:
    gpu_t() { }
    gpu_t(json &gpu_config) { (void)gpu_config; }
    virtual ~gpu_t() = default;

    virtual std::shared_ptr<desktop_plane_t> cloned_desktop() = 0;
    virtual std::shared_ptr<desktop_plane_t> shared_desktop() = 0;
    virtual qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> pinned_desktops() = 0;

    virtual void disable_display(display_plane_t &display) = 0;
    virtual void enable_display(display_plane_t &display) = 0;
};

#endif // GPU__H

