//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_RENDER_FACTORY__H
#define VM_RENDER_FACTORY__H

#include <vm_render.h>
#include <renderer.h>

class toolstack_t;

class vm_render_factory_t
{
public:
    vm_render_factory_t(renderer_t &renderer) : m_renderer(renderer)
    { }
    
    virtual ~vm_render_factory_t() = default;
    virtual std::shared_ptr<vm_render_t> make_vm_render(toolstack_t &toolstack, std::shared_ptr<vm_base_t> vm) = 0;

protected:
	renderer_t &m_renderer;
};

#endif // VM_RENDER_FACTORY__H
