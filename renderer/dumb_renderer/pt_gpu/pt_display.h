//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PT_DISPLAY__H
#define PT_DISPLAY__H

#include <display_plane.h>
// See definitions in common/include/plane.h

class pt_display_t : public display_plane_t
{
public:
    pt_display_t(rect_t rect = rect_t(0, 0, 0, 0),
           point_t plane_origin = QPoint(0, 0));
    virtual ~pt_display_t() = default;

    virtual void show_cursor(std::shared_ptr<cursor_t> cursor)
    {
        (void) cursor;
    }

    virtual void show_cursor()
    {

    }

    virtual void move_cursor(point_t point)
    {
        (void) point;
    }

    virtual void hide_cursor()
    {

    }

    virtual void dpms_on()
    {

    }

    virtual void dpms_off()
    {

    }

    virtual std::shared_ptr<framebuffer_t> framebuffer()
    {
        return nullptr;
    }

    virtual std::string name() { return m_name; }

    virtual std::shared_ptr<QImage> scratch() { return nullptr; }
};

#endif // PT_DISPLAY__H
