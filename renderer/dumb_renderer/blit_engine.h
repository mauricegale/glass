//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef BLIT_ENGINE_H
#define BLIT_ENGINE_H

#include <glass_types.h>

#include <QImage>
#include <QPainter>

class blit_engine
{
public:
    blit_engine();
    virtual ~blit_engine() = default;

    static void blit_blend(QPainter &painter,
			   const std::shared_ptr<QImage> target,
                           const rect_t &trect,
                           const std::shared_ptr<QImage> source,
                           const rectf_t &srectf,
                           const region_t &clip);

    static void blit_opaque(QPainter &painter,
			    const std::shared_ptr<QImage> target,
                            const rect_t &trect,
                            const std::shared_ptr<QImage> source,
                            const rectf_t &srectf,
                            const region_t &clip);

    static void blit_opaque(QPainter &painter,
			    const std::shared_ptr<QImage> target,
                            const rect_t &trect,
                            const QImage &source,
                            const rectf_t &srectf,
                            const region_t &clip);

};

#endif // BLIT_ENGINE_H
