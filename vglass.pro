#------------------------------------------------
#
# Project created by QtCreator 2016-10-22T17:30:34
#
#-------------------------------------------------
include(common_include.prx)

CONFIG += ordered
TEMPLATE = subdirs

#SUBDIRS += common
SUBDIRS += window_manager
SUBDIRS += renderer
SUBDIRS += input
SUBDIRS += toolstack
SUBDIRS += main

main.depends = window_manager renderer input toolstack common
input.depends = toolstack common

QMAKE_CLEAN += $$VG_BASE_DIR/lib/* $$VG_BASE_DIR/bin/glass
