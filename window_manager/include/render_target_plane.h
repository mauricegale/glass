//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef RENDER_TARGET_PLANE__H
#define RENDER_TARGET_PLANE__H

#include <render_source_plane.h>
// See definitions in common/include/plane.h

class render_target_plane_t : public plane_t
{
public:
    render_target_plane_t(rect_t rect = rect_t(0, 0, 0, 0),
                        point_t plane_origin = QPoint(0, 0));
    virtual ~render_target_plane_t();

    virtual transform_t from(render_source_plane_t &source_plane);
    virtual transform_t to(render_source_plane_t &source_plane);
    virtual transform_t translate(render_source_plane_t &source_plane);
    virtual transform_t scale(render_source_plane_t &source_plane);

    virtual point_t map_to(render_source_plane_t *source_plane, point_t point);
    virtual point_t map_from(render_source_plane_t *source_plane, point_t point);
    virtual rect_t map_to(render_source_plane_t *source_plane, rect_t rect);
    virtual rect_t map_from(render_source_plane_t *source_plane, rect_t rect);
    virtual region_t map_to(render_source_plane_t *source_plane, region_t region);
    virtual region_t map_from(render_source_plane_t *source_plane, region_t region);

    virtual void force_qemu_render_source(bool qemu);
    virtual void attach_render_source(std::shared_ptr<render_source_plane_t> render_source, bool qemu);
    virtual render_source_plane_t *render_source();
    virtual render_source_plane_t *pv_render_source();
    virtual render_source_plane_t *qemu_render_source();
    void set_key(window_key_t key);
    window_key_t key();

    virtual void set_origin(const point_t &point);
    bool is_qemu() { return !m_use_pv; }

private:
    std::shared_ptr<render_source_plane_t> m_pv_render_source;
    std::shared_ptr<render_source_plane_t> m_qemu_render_source;
    uint32_t m_edid_hash;
    window_key_t m_key;
    bool m_use_pv;
};

#endif // RENDER_TARGET_PLANE__H
