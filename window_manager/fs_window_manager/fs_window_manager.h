//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef FS_WINDOW_MANAGER__H
#define FS_WINDOW_MANAGER__H

#include <vm.h>
#include <window_manager.h>

class fs_window_manager_t : public window_manager_t
{
  Q_OBJECT
public:
    fs_window_manager_t();
    virtual ~fs_window_manager_t() = default;

    virtual window_key_t key(uuid_t uuid, render_plane_t* render_plane);

    virtual void create_render_targets(uuid_t uuid);

    virtual rect_t target(uuid_t uuid, point_t point);
    virtual rect_t clamp_rect(uuid_t uuid, point_t point, bool mouse_down);

    virtual guest_mouse_event map_event_to_guest(uuid_t uuid, point_t point, bool mouse_down, bool has_absolute_sink, bool is_touch);
    virtual void set_render_focus(uuid_t uuid)
    {
        vm_region_t *vm = m_vms[uuid].get();
        desktop_plane_t *desktop = m_input_plane.desktops()[desktop_uuid(uuid)].get();

        window_manager_t::set_render_focus(uuid);
        if(desktop && vm) {
            vm->add_dirty_rect(desktop->rect());
        }

        m_global_update = true;
    }

    template <typename Overlay>
    void process_overlays(Overlay &overlays,
                          std::shared_ptr<desktop_plane_t> &desktop,
                          std::shared_ptr<display_plane_t> &display,
                          region_t &calc_visible_region,
                          region_t &overlay_clip);

    virtual void process_updates();
    virtual list_t<uuid_t> render_focus_stack();
};

#endif //FS_WINDOW_MANAGER__H
