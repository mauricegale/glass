//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef FS_VM_REGION__H
#define FS_VM_REGION__H

#include <glass_types.h>

#include <render_target_plane.h>
#include <vm_base.h>
#include <vm_region.h>

class fs_vm_region_t : public vm_region_t
{
    Q_OBJECT
public:
    fs_vm_region_t(std::shared_ptr<vm_base_t> base) : vm_region_t(base)
    {
        m_visible_region = region_t(0,0, 32768, 32768);
        m_dirty_region = region_t(0,0, 32768, 32768);
    }
    virtual ~fs_vm_region_t() = default;

    virtual void process_updates() {}

    virtual void process_extra_vm_input(point_t point, bool mouse_down)
    {
        (void) point;
        (void) mouse_down;
    }

};

#endif // FS_VM_REGION__H
