//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "switcher_overlay.h"

switcher_overlay_t::switcher_overlay_t(uint32_t display_id, region_t region,
                                       qhash_t<uuid_t, std::shared_ptr<vm_region_t>>& vm_list,
                                       list_t<uuid_t>& vm_stack) :
    overlay_t(display_id, region_t()),
    m_icon_width(0),
    m_icon_height(0),
    m_vm_list(vm_list),
    m_vm_stack(vm_stack),
    m_highlighted_vm(uuid_t())
{
    (void)region;
}

void
switcher_overlay_t::set_highlighted_vm(uuid_t highlight_vm)
{
    m_highlighted_vm = highlight_vm;
    m_updated = true;
}

void
switcher_overlay_t::process_updates(std::shared_ptr<framebuffer_t> display)
{
    if (!m_updated || !m_visible) {
        return;
    }

    QFont font = QFont();
    double max_width = CONNECTOR_GROUP_WIDTH - VM_GAP * 2;
    double icon_width =  m_icon_width == 0 ? ICON_WIDTH_MIN : m_icon_width;
    double icon_height = m_icon_height == 0 ? ICON_WIDTH_MIN : m_icon_height;
    double scaled_icon_width = ((max_width - VM_GAP) / MAX_NUM_VMS) - VM_GAP;
    int32_t device_width = display->width();
    int32_t device_height = display->height();

    if (scaled_icon_width > ICON_WIDTH_MAX) {
        scaled_icon_width = ICON_WIDTH_MAX;
    }

    double scaled_icon_height = (scaled_icon_width / icon_width) * icon_height;
    double scaled_font_height = (FONT_HEIGHT * (scaled_icon_width / ICON_WIDTH_MIN));
    double switcher_width  = ((scaled_icon_width * m_vm_stack.size()) + (VM_GAP * m_vm_stack.size())) * 2 + VM_GAP;
    double switcher_height = (VM_GAP * 4) + scaled_icon_height + (ICON_GAP * 2) + scaled_font_height * 2;
    int32_t origin_x = (device_width / 2) - (switcher_width / 2);
    int32_t origin_y = (device_height / 2) - (switcher_height / 2);

    m_visible_region = rect_t(origin_x - ICON_GAP, origin_y - ICON_GAP, switcher_width, switcher_height);
    int i = 0;
    m_vm_icons.clear();
    for (std::list<uuid_t>::reverse_iterator rit=m_vm_stack.rbegin(); rit!=m_vm_stack.rend(); ++rit) {
        vm_icon_t vm_icon;
        rect_t name_rect;
        double center_x;
        double center_y;

        double x = origin_x + (VM_GAP + (i * (VM_GAP + scaled_icon_width)) *2);
        double y = origin_y + (VM_GAP * 2);
        double highlight_x = i * ((VM_GAP + scaled_icon_width) * 2);
        std::shared_ptr<vm_region_t> vm = m_vm_list[*rit];
        QString vm_name = QString::fromStdString(vm->base()->name());

        vm_icon.icon_image = QImage(QString::fromStdString(vm->base()->image_path()), "PNG");
        vm_icon.name = QFontMetrics(font).elidedText(vm_name, Qt::ElideRight, scaled_icon_width * 2);
        vm_icon.icon_rect = rect_t(QPoint(x, y), QSize(scaled_icon_width * 2, scaled_icon_height * 2));

        name_rect = QFontMetrics(font).boundingRect(vm_icon.name);
        center_x = scaled_icon_width - (name_rect.width() / 2) + x;
        center_y = (origin_y + scaled_font_height) + ((switcher_height - scaled_icon_height));

        vm_icon.name_origin = QPoint(center_x, center_y - (VM_GAP * 2));

        if (*rit == m_highlighted_vm) {
            m_highlight_rect = rect_t(origin_x + highlight_x, origin_y,
                                      ((scaled_icon_width + (ICON_GAP * 2)) *2), switcher_height - (ICON_GAP * 2));
        }

        m_vm_icons.push_back(vm_icon);
        i++;
    }
}

void
switcher_overlay_t::render(QPainter &painter,
                           desktop_plane_t *desktop,
                           display_plane_t *display,
                           region_t &display_clip,
                           region_t &painted_clip)
{
    (void) desktop;
    (void) display;
    (void) display_clip;
    (void) painted_clip;

    if (!m_visible) {
        return;
    }
    if (!m_updated) {
        display_clip -= m_visible_region;
        painted_clip += m_visible_region;
        display->current_clip() += m_visible_region;
        return;
    }

    m_updated = false;
    auto font = QFont();

    painter.setClipRegion(m_visible_region);
    painter.setFont(font);
    painter.setPen(QColor(qRgb(100, 100, 100)));
    painter.setBrush(QColor(qRgb(231, 231,231)));
    for (auto switcher_rect = m_visible_region.begin(); switcher_rect != m_visible_region.end(); switcher_rect++) {
        painter.drawRect(*switcher_rect);
    }

    painter.setPen(QColor(qRgb(175, 175, 175)));
    painter.setBrush(QColor(qRgb(200, 200, 200)));
    painter.drawRect(m_highlight_rect);

    painter.setPen(QColor(qRgb(25, 25, 25)));
    painter.setBrush(QColor(qRgb(231, 231, 231)));

    for (auto vm_icon : m_vm_icons) {
        painter.drawImage(vm_icon.icon_rect, vm_icon.icon_image);
        painter.drawText(vm_icon.name_origin, vm_icon.name);
    }

    display_clip -= m_visible_region;
    painted_clip += m_visible_region;
    display->current_clip() += m_visible_region;
}

void
switcher_overlay_t::show_overlay()
{
    m_updated = true;
    m_visible = true;
}

void
switcher_overlay_t::hide_overlay()
{
    m_visible = false;
}
