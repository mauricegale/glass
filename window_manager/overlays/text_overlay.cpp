//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "text_overlay.h"

text_overlay_t::text_overlay_t(uint32_t display_id, region_t region, std::string text) :
    overlay_t(display_id, region),
    m_text(text) {}

const std::string &
text_overlay_t::text()
{
  return m_text;
}

void
text_overlay_t::process_updates(std::shared_ptr<framebuffer_t> display)
{
    if (!m_updated || !m_visible) {
        return;
    }

    QFont font("DejaVu Sans", 72);
    QFontMetrics fm(font);
    QString text = QString::fromStdString(m_text);
    uint32_t text_width = fm.horizontalAdvance(text);
    uint32_t text_height = fm.height();
    point_t origin = point_t((display->width() / 2) - (text_width / 2), (4*display->height() / 5) - (text_height / 2));

    m_visible_region = rect_t(origin, QSize(text_width, text_height));
    m_text_origin = origin + point_t(0, text_height - text_height/5);
}

void
text_overlay_t::render(QPainter &painter,
                       desktop_plane_t *desktop,
                       display_plane_t *display,
                       region_t &display_clip,
                       region_t &painted_clip)
{
    (void) desktop;
    (void) display;
    (void) display_clip;
    (void) painted_clip;
    QFont font("DejaVu Sans", 72);
    QFontMetrics fm(font);

    if (!m_visible) {
        return;
    }

    if (!m_updated) {
        display_clip -= m_visible_region;
        painted_clip += m_visible_region;
        display->current_clip() += m_visible_region;
        return;
    }

    m_updated = false;
    painter.setClipRegion(m_visible_region);

    for (auto rect = m_visible_region.begin(); rect != m_visible_region.end(); rect++) {
        painter.fillRect(*rect, QColor(qRgb(200, 200, 200)));
    }

    painter.setPen(QColor(qRgb(40, 40, 40)));
    painter.setFont(font);
    painter.drawText(m_text_origin, QString::fromStdString(m_text));

    display_clip -= m_visible_region;
    painted_clip += m_visible_region;
    display->current_clip() += m_visible_region;
}

