//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <render_plane.h>

render_plane_t::render_plane_t(rect_t rect, point_t plane_origin, render_key_t key) : plane_t(rect, plane_origin), m_key(key) {}

void
render_plane_t::set_key(render_key_t key)
{
  m_key = key;
}
