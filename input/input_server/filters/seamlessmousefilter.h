//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef SEAMLESS_MOUSE_FILTER__H
#define SEAMLESS_MOUSE_FILTER__H

#include <inputserverfilter.h>

#define DISCRETE_MASK 0x80000000

class seamless_mouse_filter_t : public input_server_filter_t
{
    Q_OBJECT

public:

    static bool m_enabled;

    seamless_mouse_filter_t(std::shared_ptr<input_coordinate_mapper_t> input_mapper, QHash<std::shared_ptr<void>, std::shared_ptr<input_guest_t>> &guest_map);
    virtual ~seamless_mouse_filter_t(void);
    virtual bool filter_event(std::shared_ptr<vm_t> &vm, xt_input_event *event);

public slots:

    virtual void focused_vm_changed(std::shared_ptr<vm_t> &vm);
    virtual void displays_changed(const QList<std::shared_ptr<display_t>> &displays);
	virtual void reset_hotspot_signal(void);

signals:

    void out_of_band_event(std::shared_ptr<vm_t> target_vm, xt_input_event event);


private:

    xt_input_event event_for_seamless_position(QPoint position, std::shared_ptr<vm_t> &vm, xt_input_event original_event);
    void invalidate_mouse_buttons(QPoint last_hotspot_in_vm);
    void trackMouseButtons(const xt_input_event &event);
    std::shared_ptr<display_t> reset_hotspot(void);
    std::shared_ptr<display_t> find_default_for_vm(const std::shared_ptr<vm_t> &vm);
    std::shared_ptr<display_t> find_default_with_owner(const QString &owner);
    QPoint get_event_motion(xt_input_event *event);
    bool display_can_accept_seamless(std::shared_ptr<display_t> hpd, QPoint new_location);
    void validate_current_display(void);
    QPoint update_hotspot(xt_input_event *event);
    void update_hotspot(QPoint new_hotspot);
    xt_input_event generate_absolute_event(std::shared_ptr<display_t> display, QPoint coordinate, const xt_input_event &original_event);
    QPoint compute_truncated_movement(std::shared_ptr<display_t> current_hpd, const QPoint &new_location);
    QPoint seamless_hpd_to_gpd(std::shared_ptr<display_t> hpd, QPoint hpd_coordinate, std::shared_ptr<display_t> gpd);
    void send_out_of_band_update(void);
    void send_out_of_band_update(QPoint position, xt_input_event original_event);
    std::shared_ptr<display_t> find_equivalent_display(const QList<std::shared_ptr<display_t>> &displays, std::shared_ptr<display_t> desired_display);
    QPoint convert_to_discrete(QPoint pos, std::shared_ptr<display_t> target_hpd, std::shared_ptr<display_t> guest_hpd);
    bool handle_dead_space(std::shared_ptr<vm_t> &vm, std::shared_ptr<xt_input_event> event);


    const std::shared_ptr<input_coordinate_mapper_t> m_input_mapper;
    QHash<std::shared_ptr<void>, std::shared_ptr<input_guest_t>> &m_guest_map;
    std::shared_ptr<display_t> m_current_hpd;
    std::shared_ptr<display_t> m_current_gpd;
    QPoint m_hotspot;
    QPoint m_hotspot_hpd_coordiante;
    std::shared_ptr<vm_t> m_focused_vm;
    std::shared_ptr<vm_t> m_last_focused_vm;
    KeyMask m_active_mouse_buttons;
    xt_input_event m_null_event;
};

#endif // SEAMLESS_MOUSE_FILTER__H
