//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <seamlessmousefilter.h>

seamless_mouse_filter_t::seamless_mouse_filter_t(std::shared_ptr<input_coordinate_mapper_t> input_mapper, QHash<std::shared_ptr<void>, std::shared_ptr<input_guest_t>> &guest_map)
                                                 : m_input_mapper(input_mapper), m_guest_map(guest_map)
{
/*
    //TODO: We no longer have displaycoordinatemapper. We need this for displayschanged.
    // Subscribe to display change events from our inputMapper.
    connect(input_mapper.get(), &display_coordinate_mapper_t::displays_changed,
            this, &seamless_mouse_filter_t::displays_changed);
*/
    this->set_name("seamless");
}

seamless_mouse_filter_t::~seamless_mouse_filter_t(void)
{

}

/**
 * Discrete displays will always match host/guest resolution.  We simply need to
 * account for the offset of the hotspot
 */
point_t
seamless_mouse_filter_t::convert_to_discrete(point_t pos, std::shared_ptr<display_t> target_hpd, std::shared_ptr<display_t> guest_hpd)
{
    auto position = pos;

    // Only perform this particular translation if we're working with a discrete display.
    if (target_hpd->key() & DISCRETE_MASK) {
        //Generate a position after taking into account the x, y of the discrete hpd
        position.setX(position.x() - (target_hpd->x() - guest_hpd->x()));
        position.setY(position.y() - (target_hpd->y() - guest_hpd->y()));
    }
    return position;
}

bool
seamless_mouse_filter_t::handle_dead_space(std::shared_ptr<vm_t> &vm, std::shared_ptr<xt_input_event> event)
{
    (void)event;

    // We're in the dead space, adjust the hotspot and deliver a lastHotspotInVM for event to the guest the valid display
    if (m_current_hpd != NULL && m_current_gpd == NULL) {
        // Find the default hpd and keep sending the max x or y for the display while we're in
        // dead space so the cursor doesn't jump, vm's with deadspace only have 1 truly valid hpd.
        auto default_hpd = this->find_default_for_vm(vm);
        if (default_hpd == NULL) return false;

        /*
        auto last_hotspot_in_vm = this->compute_truncated_movement(default_hpd, m_hotspot);

        // convert into guest space
         TODO: Where is desktop_to_display defined?
          auto hpd_coordinate = m_input_mapper->desktop_to_display(last_hotspot_in_vm, default_hpd);
          if (hpd_coordinate.isNull()) return false;


         TODO: Where is gpd_for_hpd
        auto gpd = m_input_mapper->gpd_for_hpd(defaultHpd, lastHotspotInVM);
        if (gpd.isNull()) return false;


        auto gpd_coordinate = this->seamless_hpd_to_gpd(default_hpd, hpd_coordinate, gpd);
        if (gpd_coordinate.isNull()) return false;

        // send the 'max' event to keep the cursor on the edge while we're in deadspace.
        event = this->generate_absolute_event(default_hpd, gpd_coordinate, event);
        if (event == NULL) return false;
        */
        //drop the event
        return true;
    }
    return false;
}

bool
seamless_mouse_filter_t::filter_event(std::shared_ptr<vm_t> &vm, xt_input_event *event)
{
    (void)vm;
    (void)event;

    return false;
}


void
seamless_mouse_filter_t::focused_vm_changed(std::shared_ptr<vm_t> &vm)
{
    (void)vm;
}

void
seamless_mouse_filter_t::displays_changed(const QList<std::shared_ptr<display_t>> &displays)
{
    (void)displays;
}

void
seamless_mouse_filter_t::reset_hotspot_signal(void)
{

}

void
seamless_mouse_filter_t::out_of_band_event(std::shared_ptr<vm_t> target_vm, xt_input_event event)
{
    (void)target_vm;
    (void)event;
}

xt_input_event
seamless_mouse_filter_t::event_for_seamless_position(point_t position, std::shared_ptr<vm_t> &vm, xt_input_event original_event)
{
    (void)position;
    (void)vm;
    (void)original_event;

    return original_event;
}

void
seamless_mouse_filter_t::invalidate_mouse_buttons(point_t last_hotspot_in_vm)
{
    (void)last_hotspot_in_vm;
}

void
seamless_mouse_filter_t::trackMouseButtons(const xt_input_event &event)
{
    (void)event;
}

std::shared_ptr<display_t>
seamless_mouse_filter_t::reset_hotspot(void)
{
    return NULL;
}

std::shared_ptr<display_t>
seamless_mouse_filter_t::find_default_for_vm(const std::shared_ptr<vm_t> &vm)
{
    (void)vm;

    return NULL;
}

std::shared_ptr<display_t>
seamless_mouse_filter_t::find_default_with_owner(const QString &owner)
{
    (void)owner;

    return NULL;
}

point_t
seamless_mouse_filter_t::get_event_motion(xt_input_event *event)
{
    point_t p;
    (void)event;

    return p;
}

bool
seamless_mouse_filter_t::display_can_accept_seamless(std::shared_ptr<display_t> hpd, point_t new_location)
{
    (void)hpd;
    (void)new_location;

    return true;
}

void
seamless_mouse_filter_t::validate_current_display(void)
{

}

point_t
seamless_mouse_filter_t::update_hotspot(xt_input_event *event)
{
    point_t p;
    (void)event;

    return p;
}

void
seamless_mouse_filter_t::update_hotspot(point_t new_hotspot)
{
    (void)new_hotspot;
}

xt_input_event
seamless_mouse_filter_t::generate_absolute_event(std::shared_ptr<display_t> display, point_t coordinate, const xt_input_event &original_event)
{
    (void)display;
    (void)coordinate;
    (void)original_event;

    return original_event;
}

point_t
seamless_mouse_filter_t::compute_truncated_movement(std::shared_ptr<display_t> current_hpd, const point_t &new_location)
{
    (void)current_hpd;
    (void)new_location;

    return new_location;
}

point_t
seamless_mouse_filter_t::seamless_hpd_to_gpd(std::shared_ptr<display_t> hpd, point_t hpd_coordinate,std::shared_ptr<display_t> gpd)
{
    (void)hpd;
    (void)hpd_coordinate;
    (void)gpd;

    return hpd_coordinate;
}

void
seamless_mouse_filter_t::send_out_of_band_update(void)
{

}

void
seamless_mouse_filter_t::send_out_of_band_update(point_t position, xt_input_event original_event)
{
    (void)position;
    (void)original_event;
}

std::shared_ptr<display_t>
seamless_mouse_filter_t::find_equivalent_display(const QList<std::shared_ptr<display_t>> &displays, std::shared_ptr<display_t> desired_display)
{
    (void)displays;
    (void)desired_display;

    return NULL;
}
