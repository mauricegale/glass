//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef KEYSTROKE_FILTER__H
#define KEYSTROKE_FILTER__H

#include <inputserverfilter.h>
#include <inputconfigparser.h>
#include <json.hpp>

using json = nlohmann::json;

struct KeyMaskSet {
    struct KeyMask primaryMask;
    struct KeyMask secondaryMask;
    struct KeyMask whiteKeyMask;
    bool entered;
};

class key_stroke_filter_t : public input_server_filter_t
{
    Q_OBJECT

public:

    key_stroke_filter_t(
                std::shared_ptr<input_action_t> entered_action,
                std::shared_ptr<input_action_t> secondary_keyed_action,
                std::shared_ptr<input_action_t> secondary_released_action,
                std::shared_ptr<input_action_t> primary_keyed_action,
                std::shared_ptr<input_action_t> primary_released_action);
    virtual ~key_stroke_filter_t(void);
    virtual bool filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event *event);
    virtual bool set_triggers(const QString triggers_str);
    virtual bool clear_triggers();
    virtual bool add_trigger(json trigger);

signals:

    void filter_fire(const std::shared_ptr<input_action_t> &action);

private:

    bool primary_released(const struct KeyMaskSet &key_mask_set);
    bool primary_pressed(const struct KeyMaskSet &key_mask_set);
    bool secondary_released(const struct KeyMaskSet &key_mask_set);
    bool secondary_pressed(const struct KeyMaskSet &key_mask_set);
    void evaluate_entry(struct KeyMaskSet &key_mask_set);
    void add_white_list_keys(struct KeyMask &white_key_mask, const struct KeyMask &primary, const struct KeyMask &secondary);

    struct KeyMask m_last_keymask;
    struct KeyMask m_current_keymask;

    std::shared_ptr<input_action_t> m_entered_action;
    std::shared_ptr<input_action_t> m_secondary_keyed_action;
    std::shared_ptr<input_action_t> m_secondary_released_action;
    std::shared_ptr<input_action_t> m_primary_keyed_action;
    std::shared_ptr<input_action_t> m_primary_released_action;
    QList<struct KeyMaskSet> m_triggers;
    KeyMask m_global_white_key_mask;
};

#endif // INPUTSERVER_FILTER__H
