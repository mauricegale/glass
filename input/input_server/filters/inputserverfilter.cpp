//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <inputserverfilter.h>

input_server_filter_t::input_server_filter_t(void)
{
}

input_server_filter_t::~input_server_filter_t(void)
{

}

void
input_server_filter_t::update_timeout(QString timer_name, int32_t timeout)
{
    (void) timer_name;
    (void) timeout;
}

void
input_server_filter_t::set_name(QString s)
{
    m_name = s;
}

QString
input_server_filter_t::get_name()
{
    return m_name;
}

