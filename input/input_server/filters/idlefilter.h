//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef IDLE_FILTER__H
#define IDLE_FILTER__H

#include <inputserverfilter.h>
#include <QTimer>
#include <QSignalMapper>
#include <json.hpp>

using json = nlohmann::json;


template <typename T>
using list_t = std::list<T>;

struct TimerSet
{
    // Add an id so multiple idle timers can be put in place
    QString mTimerName;

    // The idle timeout value, in seconds
    uint32_t mTimeout;

    // Timer object
    std::shared_ptr<QTimer> mTimer;
};

class idle_filter_t : public input_server_filter_t
{
    Q_OBJECT

public:

    idle_filter_t();
    virtual ~idle_filter_t(void);

    /**
     * Processes the core seamless transform, which may change the
     * VM and/or event;
     */
    virtual bool filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event *event);

public slots:

    /**
     * If the timeout name matches this object, it updates the timer
     * value for this filter.
     */
    virtual void idle_timer_update(QString timer_name, int32_t timeout);
    virtual bool set_triggers(const QString triggers_str);
    virtual bool clear_triggers();
    virtual bool add_trigger(json trigger);

signals:
     /*
      * The Input Server hasn't received an event in mTimeout seconds, so
      * signal anyone subscribed to the DBus interface for an idle timeout
      * can perform whatever action necessary when the idel timer is reached
      */
     void idle_timeout(const QString &idle_name);

private slots:

     void timed_out(const QString &timer_name);

private:
    std::shared_ptr<QSignalMapper> m_signal_mapper;
    QList<struct TimerSet> m_triggers;
};

#endif // IDLE_FILTER__H
