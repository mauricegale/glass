#ifndef DBWATCH_H
#define DBWATCH_H

//
// DB Watch
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <cstdint>
#include <memory>
#include <QtCore>

// ============================================================================
// db_watch Definition
// ============================================================================

class db_dbus_t;
class db_watch : public QObject {
    Q_OBJECT

public:

    enum STATUS
    {
        STOPPED = 0,
        RUNNING = 1
    };

public:

    db_watch();
    virtual ~db_watch();

    virtual const QStringList &paths() const;
    /* deprecated */ virtual const QString path() const;
    virtual void addPath(const QString &path);
    /* deprecated */ virtual void setPath(const QString &path);
    virtual void removePath(const QString &path);

    virtual db_watch::STATUS status()const;

    virtual bool start();
    virtual void stop();

signals:

    void triggered(const QString &path);

protected slots:

    void update();

private:

    std::unique_ptr<db_dbus_t> m_db;

    std::unique_ptr<QTimer> m_timer;
    int32_t m_timeout;

    db_watch::STATUS m_status;

    QStringList m_paths;
    QList<bool> m_exists;
    QStringList m_values;
};

#endif // DBWATCH_H
