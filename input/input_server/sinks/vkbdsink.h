//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VKBD_SINK__H
#define VKBD_SINK__H

#include <guestinputsink.h>
#include <sinks/xenvkbdbackend.h>
#include <vm_base.h>  //for vg_debug
#include <QPoint>
// ============================================================================
// Description
// ============================================================================
// This sink will push events to any guest that supports using the vkbd driver
// that is maintained in the Linux kernel. Events are translated here into their
// end format 'xenkbd_in_event' which is a union of the following (from kbdif.h):
//
// Protocol definition:
// https://xenbits.xen.org/docs/unstable/hypercall/x86_64/include,public,io,kbdif.h.html
//

#define mb()     asm volatile("mfence":::"memory")
#define wmb()    asm volatile("sfence":::"memory")

using point_t = QPoint;

class vkbd_sink_t : public guest_input_sink_t
{
    Q_OBJECT

public:

    vkbd_sink_t(int32_t domid, int32_t num_devices);
    virtual ~vkbd_sink_t() = default;

    bool connected() { return m_connected; }
signals:

    void connection_status(bool is_connected);

public slots:

    void enqueue_input_event(xt_input_event event);

private slots:

    void backend_connection_status(bool is_connected);

private:

    void write_event(union xenkbd_in_event *event);
    point_t scale_absolute_event(const xt_input_event &event);

    QList<std::shared_ptr<xen_vkbd_backend_t>> m_backend_list;
    int32_t m_domid;
    uint32_t m_num_devices;
    uint32_t m_guest_width;
    uint32_t m_guest_height;

    uint32_t m_led_code;

    QString get_kbd_event_string(union xenkbd_in_event *event);

    bool m_connected = false;

};

#endif //VKBD_SINK__H
