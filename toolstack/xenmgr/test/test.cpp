//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <hippomocks.h>
#include <signal.h>
#include <unittest.h>
#include <xenmgr.h>

// NoDelete share_ptr
template <class T>
void
NoDelete(T *)
{
}

TEST_CASE("Xenmgr", "[xenmgr]")
{
    MockRepository mocks;
    std::shared_ptr<dbus_listener_t> dbus_listener(mocks.Mock<dbus_listener_t>(), NoDelete<dbus_listener_t>);

    std::shared_ptr<vm_render_factory_t> render_factory(mocks.Mock<vm_render_factory_t>(), NoDelete<vm_render_factory_t>);
    std::shared_ptr<vm_render_t> render(mocks.Mock<vm_render_t>(), NoDelete<vm_render_t>);

    std::shared_ptr<vm_region_factory_t> region_factory(mocks.Mock<vm_region_factory_t>(), NoDelete<vm_region_factory_t>);
    std::shared_ptr<vm_region_t> region(mocks.Mock<vm_region_t>(), NoDelete<vm_region_t>);

    std::shared_ptr<vm_input_factory_t> input_factory(mocks.Mock<vm_input_factory_t>(), NoDelete<vm_input_factory_t>);
    std::shared_ptr<vm_input_t> input(mocks.Mock<vm_input_t>(), NoDelete<vm_input_t>);

    uuid_t bad_uuid("00000000-0000-0000-0000-000000000000");
    QDBusObjectPath bad_path("/vm/00000000_0000_0000_0000_000000000000");
    uuid_t uivm_uuid("00000000-0000-0000-0000-000000000001");
    QDBusObjectPath uivm_path("/vm/00000000_0000_0000_0000_000000000001");

    qlist_t<uuid_t> vm_list;
    vm_list << uivm_uuid;

    mocks.OnCallFunc(seam::qobj_connect);
    mocks.OnCallFunc(make_dbus_listener).Return(dbus_listener);

    mocks.OnCall(render_factory.get(), vm_render_factory_t::make_vm_render).Return(render);
    mocks.OnCall(region_factory.get(), vm_region_factory_t::make_vm_region).Return(region);
    mocks.OnCall(input_factory.get(), vm_input_factory_t::make_vm_input).Return(input);

    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_vm_list).Return(vm_list);

    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_domid).Return(5);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_stub_domid).Return(-1);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_name).Return("uivm");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_image_path).Return("");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_long_form).Return("uivm");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_short_form).Return("uivm");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_text_color).Return("#000000");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_gpu).Return("");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_border_width).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_border_height).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_slot).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_mosaic_mode).Return(2);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_height).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_width).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_y).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_x).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_primary_domain_color).Return("#FFFFFF");
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_secondary_domain_color).Return("#FFFFFF");
    xenmgr_t manager(render_factory, region_factory, input_factory);

    SECTION("Bad list")
    {
        vm_list.push_front(bad_uuid);
        mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_vm_list).Return(vm_list);
        REQUIRE_THROWS(xenmgr_t bad_manager(render_factory, region_factory, input_factory));
    }

    SECTION("Empty list")
    {
        qlist_t<uuid_t> empty_list;
        mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_vm_list).Return(empty_list);
        REQUIRE_NOTHROW(xenmgr_t empty_manager(render_factory, region_factory, input_factory));
    }

    REQUIRE_THROWS(manager.guest_started(bad_uuid));

    REQUIRE_NOTHROW(manager.guest_started(uivm_uuid));
    REQUIRE_NOTHROW(manager.find_guest(uivm_uuid));
    REQUIRE_NOTHROW(manager.find_guest(5));

    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::set_rect);
    REQUIRE_NOTHROW(manager.set_rect(uivm_uuid, QRect(0,0,0,0)));

    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_height).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_width).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_y).Return(0);
    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_x).Return(0);
    REQUIRE(manager.get_rect(uivm_uuid) == QRect(0,0,0,0));

    REQUIRE_THROWS(manager.find_guest(bad_uuid));
    REQUIRE_THROWS(manager.find_guest(8));

    REQUIRE_THROWS(manager.guest_name_changed(bad_uuid));
    REQUIRE_NOTHROW(manager.guest_name_changed(uivm_uuid));

    REQUIRE_THROWS(manager.guest_stopped(bad_uuid));
    REQUIRE_NOTHROW(manager.guest_stopped(uivm_uuid));
    REQUIRE_THROWS(manager.find_guest(uivm_uuid));
    REQUIRE_THROWS(manager.find_guest(5));

    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_mosaic_enabled).Return(true);
    REQUIRE(manager.get_mosaic_enabled() == true);

    mocks.ExpectCall(dbus_listener.get(), dbus_listener_t::get_display_configurable).Return(true);
    REQUIRE(manager.get_display_configurable() == true);
}
