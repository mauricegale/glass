//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef TOOLSTACK__H
#define TOOLSTACK__H

#include <glass_types.h>

#include <QObject>

#include <vm.h>
#include <vm_input_factory.h>
#include <vm_region_factory.h>
#include <vm_render_factory.h>

class toolstack_t : public QObject
{
    Q_OBJECT
public:
    toolstack_t(std::shared_ptr<vm_render_factory_t> vm_render_factory,
                std::shared_ptr<vm_region_factory_t> vm_region_factory,
                std::shared_ptr<vm_input_factory_t> vm_input_factory) { (void)vm_render_factory; (void)vm_region_factory; (void)vm_input_factory;}
    virtual ~toolstack_t() = default;

    /**
     * Finds the VM object for the guest with the provided UUID,
     * if it exists.
     *
     * @param uuid The UUID object for the guest's UUID.
     * @return a VM object, throws an exception if guest not found.
     */
    virtual std::shared_ptr<vm_t> find_guest(const uuid_t uuid) = 0;

    /**
     * Finds the VM object for the guest with the provided domid,
     * if it exists.
     *
     * @param domid The domid of the running VM.
     * @return a VM object, throws an exception if guest not found.
     */
    virtual std::shared_ptr<vm_t> find_guest(const domid_t domid) = 0;

    virtual void reset() = 0;

signals:
    void add_guest(std::shared_ptr<vm_t> guest);
    void update_guest(std::shared_ptr<vm_t> guest);
    void remove_guest(std::shared_ptr<vm_t> guest);
    void reboot_guest(const uuid_t uuid);
    void guest_online(const vm_t &guest);
    void guest_offline(const vm_t &guest);
};

#endif // TOOLSTACK__H
