//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM__H
#define VM__H

#include <glass_types.h>

#include <vm_input.h>
#include <vm_region.h>
#include <vm_render.h>

#include <QObject>

#include <qobj.h>

class vm_t : public QObject
{
    Q_OBJECT

public:
    vm_t(std::shared_ptr<vm_base_t> base = nullptr,
         std::shared_ptr<vm_render_t> render = nullptr,
         std::shared_ptr<vm_region_t> region = nullptr,
         std::shared_ptr<vm_input_t> input = nullptr) : m_base(base),
                                                        m_render(render),
                                                        m_region(region),
                                                        m_input(input)
    {
        seam::qobj_connect(m_render.get(), SIGNAL(dirty_rect(rect_t)),
                           m_region.get(), SLOT(add_dirty_rect(rect_t)));
    }

    vm_t(vm_t &&) = default;
    vm_t &operator=(vm_t &&) = default;
    vm_t(const vm_t &) = default;
    vm_t &operator=(const vm_t &) = default;

    virtual ~vm_t() = default;

    virtual const std::string name(void) const
    {
        return m_base->name();
    }

    virtual domid_t domid(void) const
    {
        return m_base->domid();
    }
    virtual domid_t stub_domid(void) const
    {
        return m_base->stub_domid();
    }

    virtual const uuid_t uuid(void) const
    {
        return m_base->uuid();
    }

    virtual const std::string primary_domain_color(void) const
    {
        return m_base->primary_domain_color();
    }

    virtual const std::string image_path(void) const
    {
        return m_base->image_path();
    }

    virtual const std::string long_form(void) const
    {
        return m_base->long_form();
    }

    virtual const std::string short_form(void) const
    {
        return m_base->short_form();
    }

    virtual const std::string text_color(void) const
    {
        return m_base->text_color();
    }

    virtual const std::string gpu(void) const
    {
        return m_base->gpu();
    }

    virtual int border_width(void) const
    {
        return m_base->border_width();
    }

    virtual int border_height(void) const
    {
        return m_base->border_height();
    }

    virtual int slot(void) const
    {
        return m_base->slot();
    }

    virtual rect_t rect(void) const
    {
        return m_base->rect();
    }

    virtual void set_rect(rect_t rect)
    {
        emit set_db_rect(m_base->uuid(), rect);
    }

    const std::shared_ptr<vm_base_t> base()
    {
        return m_base;
    }

    const std::shared_ptr<vm_render_t> render()
    {
        return m_render;
    }

    const std::shared_ptr<vm_region_t> region()
    {
        return m_region;
    }

    const std::shared_ptr<vm_input_t> input()
    {
        return m_input;
    }

public slots:
  virtual void update_rect(rect_t rect) { set_rect(rect); }

signals:
    void set_db_rect(uuid_t uuid, QRect rect);

protected:
    // Holds the domid, stubdomid, uuid and name of the vm
    // Used by: all
    std::shared_ptr<vm_base_t> m_base;

    // Holds renderer specific information, that is used for
    // displaying information associated with the vm
    // Used by: renderer
    std::shared_ptr<vm_render_t> m_render;

    // Holds window management information specific to the vm
    // Used by: window_manager module
    std::shared_ptr<vm_region_t> m_region;

    // Holds input specific information, that is used for
    // managing the behavior of a vm's input interactions
    // Used by: input module
    std::shared_ptr<vm_input_t> m_input;
};

#endif // VM_H
